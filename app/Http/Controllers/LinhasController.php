<?php

namespace App\Http\Controllers;

use App\Models\Linhas;
use Illuminate\Http\Request;

class LinhasController extends Controller
{
    //
    function index(){
        try{
            $linhas = Linhas::all();
            return response()->json(['data' => $linhas]);
        }catch(\Throwable $errr){

        }
    }

    function find($id){
        try{
            $linha = Linhas::find($id);
            return response()->json(['data' => $linha]); 
        }catch(\Throwable $errr){

        }
    }
}
