<?php

use App\Http\Controllers\LinhasController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/linhas', [LinhasController::class, 'index'])->name('linhas');
Route::get('/linhas/{id}', [LinhasController::class, 'find'])->name('linhas.find');

